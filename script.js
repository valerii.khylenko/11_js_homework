const btnPass = document.querySelector('.btn');
const formPass = document.querySelector('.password-form');
formPass.addEventListener('click', (event) => {
    if (event.target.classList.contains('icon-password')) {
        if (event.target.classList.contains('fa-eye')) {
            event.target.classList.replace('fa-eye', 'fa-eye-slash');
            event.target.parentElement.querySelector('input').type = 'text';
        }
        else {
            event.target.className = event.target.classList.replace('fa-eye-slash', 'fa-eye');
            event.target.parentElement.querySelector('input').type = 'password';
        }
    }
});
const getError = document.createElement('p');
getError.textContent = 'Потрібно ввести однакові значенняя';
getError.classList.add('error');

btnPass.addEventListener('click', () => {
    const input = document.querySelector('.password')
    const confirm = document.querySelector('.confirm-password')
    if (input.value === confirm.value) {
        getError.remove();
        alert('You are welcome!')
    } else {
        document.querySelector('button').before(getError);
    }
});